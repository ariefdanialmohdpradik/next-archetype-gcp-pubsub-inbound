package th.co.ktb.next.archetype.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.gcp.pubsub.integration.AckMode;
import org.springframework.cloud.gcp.pubsub.integration.inbound.PubSubInboundChannelAdapter;
import org.springframework.cloud.gcp.pubsub.support.BasicAcknowledgeablePubsubMessage;
import org.springframework.cloud.gcp.pubsub.support.GcpPubSubHeaders;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;
import th.co.ktb.next.archetype.model.request.SampleApiRequest;
import th.co.ktb.next.archetype.model.response.SampleApiResponse;
import th.co.ktb.next.common.service.BaseService;
import th.co.ktb.next.telemetry.interceptor.PubSubInboundInterceptor;

/**
 * This GET service will read & acknowledge messages from MySubscription (topic = MySampleTopic)
 */

@Log4j2
@Service
public class GetMessageAService implements BaseService<SampleApiRequest, SampleApiResponse> {

    @Value("${spring.cloud.gcp.subscriptions.subscription_1}")
    private String SUBSCRIPTION_NAME;

    private PubSubInboundInterceptor pubSubInboundInterceptor = new PubSubInboundInterceptor();

    // Listening to messages from GCP Pub/Sub subscription and send to channel

    @Bean
    private PubSubInboundChannelAdapter messageChannelAdapter(@Qualifier("myInputChannel") MessageChannel inputChannel,
                                                             PubSubTemplate pubSubTemplate) {

        PubSubInboundChannelAdapter adapter = new PubSubInboundChannelAdapter(pubSubTemplate, SUBSCRIPTION_NAME);
        adapter.setOutputChannel(inputChannel);
        adapter.setAckMode(AckMode.MANUAL); // declare as manual unless def as auto (acknowledgement defined manually in service)
        adapter.setPayloadType(SampleApiRequest.class);
        return adapter;
    }

    // Process incoming messages

    @ServiceActivator(inputChannel = "myInputChannel")
    public void getMessage(@Header(GcpPubSubHeaders.ORIGINAL_MESSAGE) BasicAcknowledgeablePubsubMessage messageAck,
                            SampleApiRequest sampleApiRequest) {

//        if (sampleApiRequest.getMessage().equals("hello")) {}
        pubSubInboundInterceptor.intercept(SUBSCRIPTION_NAME, sampleApiRequest);
        messageAck.ack();
    }

    /*
     * If you want to acknowledge messages manually, you need to provide BasicAcknowledgeablePubSubMessage,
     * Hence creating a custom execute func
     **/
    public void executeCustom(@Header(GcpPubSubHeaders.ORIGINAL_MESSAGE) BasicAcknowledgeablePubsubMessage messageAck,
                              SampleApiRequest sampleApiRequest) {
        pubSubInboundInterceptor.intercept(SUBSCRIPTION_NAME, sampleApiRequest);
        messageAck.ack();
    }

//    @Override
//    public SampleApiResponse execute(SampleApiRequest sampleApiRequest) {
//        pubSubInboundInterceptor.intercept(SUBSCRIPTION_NAME, sampleApiRequest);
//        return new SampleApiResponse();
//    }

    @Override
    public SampleApiResponse execute(SampleApiRequest sampleApiRequest) {
        return new SampleApiResponse();
    }
}
