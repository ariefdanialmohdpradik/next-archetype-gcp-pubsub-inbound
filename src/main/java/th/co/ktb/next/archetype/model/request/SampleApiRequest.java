package th.co.ktb.next.archetype.model.request;

import lombok.*;
import th.co.ktb.next.common.base.BaseRequest;
//import th.co.ktb.next.common.base.BaseRequest;

/*
* This is a sample model for request of an API.
* 1. The request model must implement the BaseRequest interface.
* 2. This class serves as input of the respective API.
* */

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SampleApiRequest extends BaseRequest {
    private String topicName;
    private String message;
    private String message1;
}
