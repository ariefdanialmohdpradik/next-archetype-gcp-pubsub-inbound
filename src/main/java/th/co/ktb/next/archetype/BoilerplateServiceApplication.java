package th.co.ktb.next.archetype;

//import th.co.ktb.next.starter.annotations.EnableSwagger;
//import th.co.ktb.next.starter.annotations.EnableTelemetry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/*
* This class serves as main application.
* It provides Telemetry and Swagger capability with @EnableTelemetry and @EnableSwagger
* */
@SpringBootApplication
//@EnableTelemetry
//@EnableSwagger
public class BoilerplateServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BoilerplateServiceApplication.class, args);
	}

}
