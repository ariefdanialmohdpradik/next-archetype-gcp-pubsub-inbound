package th.co.ktb.next.archetype.model.response;

import lombok.*;
import th.co.ktb.next.common.base.BaseResponse;
//import th.co.ktb.next.common.base.BaseResponse;

/*
 * This is a sample model for response of an API.
 * 1. The response model must implement the BaseResponse interface.
 * 2. This class serves as output of the respective API.
 * */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SampleApiResponse extends BaseResponse {
    private String topicName;
    private String message;
    private String message1;
}