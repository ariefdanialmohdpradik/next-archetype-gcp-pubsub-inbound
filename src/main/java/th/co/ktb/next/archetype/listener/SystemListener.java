package th.co.ktb.next.archetype.listener;

import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.gcp.pubsub.support.BasicAcknowledgeablePubsubMessage;
import org.springframework.cloud.gcp.pubsub.support.GcpPubSubHeaders;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;
import th.co.ktb.next.archetype.model.request.SampleApiRequest;
import th.co.ktb.next.archetype.model.response.SampleApiResponse;
import th.co.ktb.next.archetype.service.GetMessageAService;

@Log4j2
@Component
public class SystemListener {

    private GetMessageAService getMessageAService;

    public SystemListener(GetMessageAService getMessageAService) {
        this.getMessageAService = getMessageAService;
    }

    // Main
//    @ServiceActivator(inputChannel = "myInputChannel")
//    public void getMessage(@Header(GcpPubSubHeaders.ORIGINAL_MESSAGE) BasicAcknowledgeablePubsubMessage messageAck,
//                                        SampleApiRequest sampleApiRequest) {
//
//
////        getMessageFromGcpService.executeCustom(messageAck, sampleApiRequest);
//
//        getMessageAService.execute(sampleApiRequest);
//        messageAck.ack();
//    }

    // Another option
    public SampleApiResponse getMessage(SampleApiRequest sampleApiRequest) {
        return getMessageAService.execute(sampleApiRequest);
    }
}
